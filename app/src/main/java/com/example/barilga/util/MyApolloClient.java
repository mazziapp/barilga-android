package com.example.barilga.util;

import android.app.Application;


import com.apollographql.apollo.ApolloClient;
import com.example.barilga.profile.ProfilePreferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class MyApolloClient extends Application {

    private static ApolloClient apolloClient;
    private static String token, url = "http://192.168.1.10:10002/graphql";
    private static OkHttpClient createOkHttpWithValidToken() {
        if(ProfilePreferences.getInstance().getToken() == null){
            token = "";
        }
        else{
            token = ProfilePreferences.getInstance().getToken();
        }
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        return chain.proceed(chain.request().newBuilder().header("x-access-token", token).build());
                    }
                }).build();
    }

    public static ApolloClient getMyApolloClient() {

        apolloClient = ApolloClient.builder()
                .serverUrl(url)
                .okHttpClient(createOkHttpWithValidToken())
                .build();
        return apolloClient;
    }
}