package com.example.barilga.util;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MyDateFormat {

    public static String generate(long d) {
        generate(d, "yy/MM/dd hh:mm");
        try {
            DateFormat dateFormat = new SimpleDateFormat("yy/MM/dd hh:mm", Locale.ENGLISH);
            Date date = (new Date(d));
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String generate(long d, String mFormat) {
        try {
            DateFormat dateFormat = new SimpleDateFormat(mFormat, Locale.ENGLISH);
            Date date = (new Date(d));
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    public static String getDayOfWeek(long d) {
        String mFormat = "yyyy/MM/dd";
        DateFormat dateFormat = new SimpleDateFormat(mFormat, Locale.ENGLISH);
        Date date = (new Date(d));
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int day = c.get(Calendar.DAY_OF_WEEK);
        long now = Calendar.getInstance().getTimeInMillis();
        int diff = (int) ((now - d) / 1000 / 3600 / 24);
        Log.i("TIME TRAVEL", "NOW :" + now / 1000 / 3600 + " DATE : " + d / 1000 / 3600 + " DIFF : " + diff);
        String dayOfWeek = "";
        if (diff > 6) {
            dayOfWeek = dateFormat.format(date);
        } else
        {
            switch (day) {
                case 2:
                    dayOfWeek = "Даваа";
                    break;
                case 3:
                    dayOfWeek = "Мягмар";
                    break;
                case 4:
                    dayOfWeek = "Лхагва";
                    break;
                case 5:
                    dayOfWeek = "Пүрэв";
                    break;
                case 6:
                    dayOfWeek = "Баасан";
                    break;
                case 7:
                    dayOfWeek = "Бямба";
                    break;
                case 1:
                    dayOfWeek = "Ням";
                    break;
            }
        }

        return dayOfWeek;
    }
    public static long currentTime() {
        return (new Date()).getTime();
    }

}