package com.example.barilga.Category;

public class Category {
    private String categoryId;
    private String user_id;
    private String categoryName;
    private String categoryImage;
    private String parentId;
    private Integer sortOrder;
    private Double updatedAt;
    private Double createdAt;

    public Category(String categoryId, String user_id, String categoryName, String categoryImage, String parentId, Integer sortOrder, Double updatedAt, Double createdAt) {
        this.categoryId = categoryId;
        this.user_id = user_id;
        this.categoryName = categoryName;
        this.categoryImage = categoryImage;
        this.parentId = parentId;
        this.sortOrder = sortOrder;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Double getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Double updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Double getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Double createdAt) {
        this.createdAt = createdAt;
    }
}
