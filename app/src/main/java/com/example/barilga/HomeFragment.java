package com.example.barilga;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.barilga.products.Product;
import com.example.barilga.products.ProductAdapter;
import com.example.barilga.products.ProductsFragment;

import java.util.ArrayList;


public class HomeFragment extends Fragment {
    private TextView txtNotificationCount;
    private SearchView searchView;
    private ImageButton btnNotification;
    private RecyclerView recyclerSpecialProducts;
    private ArrayList<Product> specialProducts = new ArrayList<>();
    private ProductAdapter productAdapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
        Product product = new Product("111","798797","7744646","0001","Бахь","Jobi","IKEA","Чанар бол бидний зорилт",15000,14000,"qwer","cash only",100,new ArrayList<String>(),5,true,12315615.0,18151321.0);
        Product product1 = new Product("112","798798","7744647","0002","Хадаас","100 ail","Top nails","Хззээ ч зэврэхгүй ган хадаас",500,400,"qwerfff","no delivery",100,new ArrayList<String>(),4,true,12315615.0,18151321.0);
        Product product2 = new Product("113","798799","7744648","0003","Паркэтэн шал","Hermes","Russ","Орос чанарыг танай гэрт",5000,4500,"qwerdf","300+ boonii vneer bodoj hvrgej ogno",100000,new ArrayList<String>(),5,true,12315615.0,18151321.0);
        specialProducts.add(product);
        specialProducts.add(product1);
        specialProducts.add(product2);
        specialProducts.add(product2);
//        initializeRecyclerView();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container,@android.support.annotation.Nullable Bundle savedInstances){
        View view = inflater.inflate(R.layout.fragment_home,container,false);
        txtNotificationCount = view.findViewById(R.id.txtNotificationCount);
        btnNotification = view.findViewById(R.id.btnNotification);
        recyclerSpecialProducts = view.findViewById(R.id.recyclerSpecialProducts);
        txtNotificationCount.setText("10");
        return view;
    }

    @Override
    public void onActivityCreated(@android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        initializeRecyclerView();
    }

    private void initializeRecyclerView(){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                productAdapter = new ProductAdapter(specialProducts);
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);;
                recyclerSpecialProducts.setLayoutManager(layoutManager);
                recyclerSpecialProducts.setItemAnimator(new DefaultItemAnimator());
                recyclerSpecialProducts.setAdapter(productAdapter);
            }
        });
    }
}
