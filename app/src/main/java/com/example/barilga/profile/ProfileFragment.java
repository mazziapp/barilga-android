package com.example.barilga.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.barilga.Authentication.LoginActivity;
import com.example.barilga.LogOutQuery;
import com.example.barilga.R;
import com.example.barilga.SignOutQuery;
import com.example.barilga.Store.AddStoreActivity;
import com.example.barilga.Store.StoresActivity;
import com.example.barilga.util.MyApolloClient;
import com.google.firebase.auth.FirebaseAuth;

import org.jetbrains.annotations.NotNull;

public class ProfileFragment extends Fragment {
    private TextView txtEmail, txtName, txtNotificationCount;
    private Button btnEditProfile, btnLogout;
    private LinearLayout linearName, linearShopAddress, linearShopSettings, linearAddShop, linearBalanceSettings;
    private ImageButton btnNotification;
    @Override
    public void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @android.support.annotation.Nullable Bundle savedInstances){
        View view = inflater.inflate(R.layout.fragment_profile,container,false);
        txtEmail = view.findViewById(R.id.txtEmail);
        txtName = view.findViewById(R.id.txtName);
        txtNotificationCount = view.findViewById(R.id.txtNotificationCount);
        btnEditProfile = view.findViewById(R.id.btnEditProfile);
        btnNotification = view.findViewById(R.id.btnNotification);
        btnLogout = view.findViewById(R.id.btnLogout);
        linearAddShop = view.findViewById(R.id.linearLayoutAddShop);
        linearBalanceSettings = view.findViewById(R.id.linearLayoutBalanceSettings);
        linearName = view.findViewById(R.id.linearLayoutName);
        linearShopAddress = view.findViewById(R.id.linearLayoutShopAddress);
        linearShopSettings = view.findViewById(R.id.linearLayoutShopSettings);
        linearShopSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopSettings();
            }
        });

        linearAddShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addShop();
            }
        });

        linearBalanceSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                balanceSettings();
            }
        });

        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });

        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNotificationsActivity();
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        if(!ProfilePreferences.getInstance().getNickname().isEmpty()){
            txtName.setText(ProfilePreferences.getInstance().getNickname());
        }
        return view;
    }

    @Override
    public void onActivityCreated(@android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void editProfile(){

    }

    public void addShop(){
        Intent intent = new Intent(getContext(), AddStoreActivity.class);
        startActivity(intent);
    }

    public void balanceSettings(){

    }

    public void shopSettings(){
        Intent intent = new Intent(getContext(), StoresActivity.class);
        startActivity(intent);
    }

    public void openNotificationsActivity(){

    }

    public void logout(){
        Log.v("checkUserId","userId = " + ProfilePreferences.getInstance().getId());
        MyApolloClient.getMyApolloClient().query(SignOutQuery.builder()
        .userId(ProfilePreferences.getInstance().getId())
        .build()).enqueue(new ApolloCall.Callback<SignOutQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<SignOutQuery.Data> response) {
                try{
                    Log.v("checkLogout","response = " + response.data().signOut().toString());
                    if(response.data().signOut().message().equals("OK")){
                        FirebaseAuth.getInstance().signOut();
                        ProfilePreferences.getInstance().clear();
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        getActivity().finish();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }
}
