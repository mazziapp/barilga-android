package com.example.barilga.products;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.barilga.AddProductMutation;
import com.example.barilga.Category.Category;
import com.example.barilga.GetCategoriesQuery;
import com.example.barilga.R;
import com.example.barilga.profile.ProfilePreferences;
import com.example.barilga.type.InputProduct;
import com.example.barilga.util.MyApolloClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AddProductsActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView txtToolbarTitle;
    private EditText inputProductName, inputPriceFew, inputPriceLot, inputProductDescription;
    private Button btnAddProduct;
    private Spinner spinnerCategory;
    private ArrayList<Category> categories = new ArrayList<>();
    private String storeId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_add_products);
        storeId = getIntent().getStringExtra("storeId");
        txtToolbarTitle = findViewById(R.id.toolbar_title);
        inputPriceFew = findViewById(R.id.inputPriceFew);
        inputPriceLot = findViewById(R.id.inputPriceLot);
        inputProductName = findViewById(R.id.inputProductName);
        inputProductDescription = findViewById(R.id.inputProductDescription);
        spinnerCategory = findViewById(R.id.spinnerCategory);
        btnAddProduct = findViewById(R.id.btnAddProduct);
        btnAddProduct.setOnClickListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getCategories();
    }

    public void addProduct(){
        String productName = inputProductName.getText().toString();
        String productDescription = inputProductDescription.getText().toString();
        Integer priceFew = Integer.valueOf(inputPriceFew.getText().toString());
        Integer priceLot = Integer.valueOf(inputPriceLot.getText().toString());
        ArrayList<String> images = new ArrayList<>();
        int index = spinnerCategory.getSelectedItemPosition();
        String categoryId = categories.get(index).getCategoryId();
        InputProduct inputProduct = InputProduct.builder()
                .product_code("")
                .product_name(productName)
                .product_manufacturer("")
                .product_description(productDescription)
                .product_price(priceFew)
                .product_sale_price(priceLot)
                .product_measure("")
                .product_condition("")
                .product_quantity(0)
                .product_image(images)
                .product_rating(0)
                .build();

        MyApolloClient.getMyApolloClient().mutate(AddProductMutation.builder()
        .userId(ProfilePreferences.getInstance().getId())
        .storeId(storeId)
        .categoryId(categoryId)
        .inputProduct(inputProduct)
        .build()).enqueue(new ApolloCall.Callback<AddProductMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<AddProductMutation.Data> response) {
                try{
                    Log.v("checkAddProduct","response = " + response.data().addProduct().toString());
                    if(response.data().addProduct().message().equals("OK")){
                        showToast();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }

    public void getCategories(){
        MyApolloClient.getMyApolloClient().query(GetCategoriesQuery.builder()
        .build()).enqueue(new ApolloCall.Callback<GetCategoriesQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<GetCategoriesQuery.Data> response) {
                try{
                    Log.v("checkCategories","response = " + response.data().getCategories().toString());
                    if(response.data().getCategories().message().equals("OK")){
                        for(int i = 0; i < response.data().getCategories().result().size(); i++){
                            String categoryId = response.data().getCategories().result().get(i)._id().toString();
                            String userId = response.data().getCategories().result().get(i).user_id().toString();
                            String categoryName = response.data().getCategories().result().get(i).category_name();
                            String categoryImage = response.data().getCategories().result().get(i).category_image();
                            String parentId = response.data().getCategories().result().get(i).parent_id();
                            Integer sortOrder = response.data().getCategories().result().get(i).sort_order();
                            Double upDatedAt = response.data().getCategories().result().get(i).updated_at();
                            Double createdAt = response.data().getCategories().result().get(i).created_at();
                            Category category = new Category(categoryId,userId,categoryName,categoryImage,parentId,sortOrder,upDatedAt,createdAt);
                            categories.add(category);
                        }
                        initializeSpinner();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }

    public void initializeSpinner(){
        final ArrayList categoryString = new ArrayList();
        for(int i = 0; i < categories.size(); i++){
            categoryString.add(categories.get(i).getCategoryName());
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddProductsActivity.this, R.layout.spinner_item_white, categoryString);
                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(R.layout.spinner_item_dropdown);
                // attaching data adapter to spinner
                spinnerCategory.setAdapter(dataAdapter);
            }
        });
    }

    public void showToast(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AddProductsActivity.this,"Бараа амжилттай нэмэгдлээ",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAddProduct:
                addProduct();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
