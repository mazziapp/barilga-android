package com.example.barilga.products;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barilga.Cart.CartActivity;
import com.example.barilga.Cart.CartItem;
import com.example.barilga.MainActivity;
import com.example.barilga.R;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txtProductName, txtPrice, txtRating, txtSellerName;
    private ImageView imgThumbnail, imgCart;
    private Button btnSave, btnAddToCart;
    private Product product;

    @Override
    protected void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_product_detail);
        txtProductName = findViewById(R.id.toolbar_title);
        txtPrice = findViewById(R.id.txtPrice);
        txtRating = findViewById(R.id.txtRating);
        txtSellerName = findViewById(R.id.txtSellerName);
        imgThumbnail = findViewById(R.id.imgThumbnail);
        imgCart = findViewById(R.id.imgCart);
        imgCart.setOnClickListener(this);
        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        btnAddToCart = findViewById(R.id.btnAddToCart);
        btnAddToCart.setOnClickListener(this);
        product = getIntent().getParcelableExtra("product");
        txtProductName.setText(product.getName());
        txtPrice.setText(product.getPrice().toString());
        txtSellerName.setText(product.getBrandName());
        txtRating.setText(product.getRating().toString());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void addToCart(){
        CartItem cartItem = new CartItem(product,1);
        MainActivity.handleCartItems(cartItem);
        Toast.makeText(getApplicationContext(),"Бараа сагсанд нэмэгдлээ",Toast.LENGTH_SHORT).show();
    }

    public void save(){
        Toast.makeText(getApplicationContext(),"Saved",Toast.LENGTH_SHORT).show();
    }

    public void openCartActivity(){
        Intent intent = new Intent(ProductDetailActivity.this, CartActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSave:
                save();
                break;
            case R.id.btnAddToCart:
                addToCart();
                break;
            case R.id.imgCart:
                openCartActivity();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
