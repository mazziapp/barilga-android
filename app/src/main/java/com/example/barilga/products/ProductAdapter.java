package com.example.barilga.products;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.barilga.R;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {
    private List<Product> products;
    private Context mContext;

    public ProductAdapter(List<Product> products){
        this.products = products;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_product,viewGroup,false);
        mContext = viewGroup.getContext();
        return new ProductAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int i) {
        final Product product = products.get(i);
        viewHolder.txtProductName.setText(product.getName());
        viewHolder.txtPrice.setText(product.getPrice().toString());
        viewHolder.txtRating.setText(product.getRating().toString());
        viewHolder.itemView.setOnClickListener(new MyViewHolderOnClickListener(product,viewHolder));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtProductName, txtPrice, txtRating;
        ImageView imgThumbnail;

        MyViewHolder(View itemView){
            super(itemView);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtRating = itemView.findViewById(R.id.txtRating);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            imgThumbnail = itemView.findViewById(R.id.imgThumbnail);
        }
    }

    private static class MyViewHolderOnClickListener implements View.OnClickListener{
        private final Product product;
        private MyViewHolder holder;
        private MyViewHolderOnClickListener(Product product, MyViewHolder holder) {
            this.holder = holder;
            this.product = product;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(),ProductDetailActivity.class);
            intent.putExtra("product",(Parcelable) product);
            v.getContext().startActivity(intent);
        }
    }
}
