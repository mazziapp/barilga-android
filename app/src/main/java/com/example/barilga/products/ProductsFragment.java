package com.example.barilga.products;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TextView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.barilga.GetProductsByStoreIdQuery;
import com.example.barilga.R;
import com.example.barilga.util.MyApolloClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class ProductsFragment extends Fragment {
    private TextView txtTest;
    private Button btnAddProduct;
    private SearchView searchView;
    private ArrayList<Product> products = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @android.support.annotation.Nullable Bundle savedInstances){
        View view = inflater.inflate(R.layout.fragment_products,container,false);
        txtTest = view.findViewById(R.id.textView);
        btnAddProduct = view.findViewById(R.id.btnAddProduct);
        btnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(),AddProductsActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void getProducts(){
        MyApolloClient.getMyApolloClient().query(GetProductsByStoreIdQuery.builder()
        .storeId("")
        .build()).enqueue(new ApolloCall.Callback<GetProductsByStoreIdQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<GetProductsByStoreIdQuery.Data> response) {
                try{
                    Log.v("checkProducts","response = " + response.data().getProductsByStoreId().toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }
}
