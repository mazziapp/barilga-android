package com.example.barilga.products;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable, Parcelable{
    private String productId;
    private String storeId;
    private String categoryId;
    private String code;
    private String name;
    private String manufacturer;
    private String brandName;
    private String description;
    private Integer price;
    private Integer salePrice;
    private String measure;
    private String condition;
    private Integer quantity;
    private ArrayList<String> image;
    private Integer rating;
    private Boolean isApproved;
    private Double updatedAt;
    private Double createdAt;

    public Product(String productId, String storeId, String categoryId, String code, String name, String manufacturer, String brandName, String description, Integer price, Integer salePrice, String measure, String condition, Integer quantity, ArrayList<String> image, Integer rating, Boolean isApproved, Double updatedAt, Double createdAt) {
        this.productId = productId;
        this.storeId = storeId;
        this.categoryId = categoryId;
        this.code = code;
        this.name = name;
        this.manufacturer = manufacturer;
        this.brandName = brandName;
        this.description = description;
        this.price = price;
        this.salePrice = salePrice;
        this.measure = measure;
        this.condition = condition;
        this.quantity = quantity;
        this.image = image;
        this.rating = rating;
        this.isApproved = isApproved;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
    }

    protected Product(Parcel in) {
        productId = in.readString();
        storeId = in.readString();
        categoryId = in.readString();
        code = in.readString();
        name = in.readString();
        manufacturer = in.readString();
        brandName = in.readString();
        description = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readInt();
        }
        if (in.readByte() == 0) {
            salePrice = null;
        } else {
            salePrice = in.readInt();
        }
        measure = in.readString();
        condition = in.readString();
        if (in.readByte() == 0) {
            quantity = null;
        } else {
            quantity = in.readInt();
        }
        image = in.createStringArrayList();
        if (in.readByte() == 0) {
            rating = null;
        } else {
            rating = in.readInt();
        }
        byte tmpIsApproved = in.readByte();
        isApproved = tmpIsApproved == 0 ? null : tmpIsApproved == 1;
        if (in.readByte() == 0) {
            updatedAt = null;
        } else {
            updatedAt = in.readDouble();
        }
        if (in.readByte() == 0) {
            createdAt = null;
        } else {
            createdAt = in.readDouble();
        }
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ArrayList<String> getImage() {
        return image;
    }

    public void setImage(ArrayList<String> image) {
        this.image = image;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }

    public Double getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Double updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Double getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Double createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productId);
        dest.writeString(storeId);
        dest.writeString(categoryId);
        dest.writeString(code);
        dest.writeString(name);
        dest.writeString(manufacturer);
        dest.writeString(brandName);
        dest.writeString(description);
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(price);
        }
        if (salePrice == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(salePrice);
        }
        dest.writeString(measure);
        dest.writeString(condition);
        if (quantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(quantity);
        }
        dest.writeStringList(image);
        if (rating == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(rating);
        }
        dest.writeByte((byte) (isApproved == null ? 0 : isApproved ? 1 : 2));
        if (updatedAt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(updatedAt);
        }
        if (createdAt == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(createdAt);
        }
    }
}
