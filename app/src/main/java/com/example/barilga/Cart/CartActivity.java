package com.example.barilga.Cart;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.example.barilga.MainActivity;
import com.example.barilga.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class CartActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView txtCartEmpty, txtTotalPrice, txtFinalPrice;
    private Button btnCheckout;
    private RecyclerView recyclerCartItems, recyclerSelectedItems;
    private CartItemAdapter cartItemAdapter;
    private PopupWindow mPopupWindow;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_cart);
        recyclerCartItems = findViewById(R.id.recyclerViewCartItems);
        txtCartEmpty = findViewById(R.id.txtCartEmpty);
        txtTotalPrice = findViewById(R.id.txtTotalPrice);
        linearLayout = findViewById(R.id.linearLayout);
        btnCheckout = findViewById(R.id.btnCheckout);
        btnCheckout.setOnClickListener(this);
        EventBus.getDefault().register(this);
        initialize();
    }

    @Subscribe
    public void onEvent(CartItem cartItem) {
            cartItemAdapter.notifyDataSetChanged();
            initialize();
    }

    public void checkout(){
        checkoutMenu();
    }

    public void initializeRecyclerView(){
        cartItemAdapter = new CartItemAdapter(MainActivity.cartItems,0);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerCartItems.setLayoutManager(layoutManager);
        recyclerCartItems.setItemAnimator(new DefaultItemAnimator());
        recyclerCartItems.setAdapter(cartItemAdapter);
        calculateTotalPrice();
    }

    public void initializeSelectedRecyclerView(){
//        cartItemAdapter = new CartItemAdapter(MainActivity.cartItems,0);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerSelectedItems.setLayoutManager(layoutManager);
        recyclerSelectedItems.setItemAnimator(new DefaultItemAnimator());
        recyclerSelectedItems.setAdapter(cartItemAdapter);
    }

    public void initialize(){
        if(MainActivity.cartItems.size() <=0 ){
            txtCartEmpty.setVisibility(View.VISIBLE);
        }
        else{
            txtCartEmpty.setVisibility(View.INVISIBLE);
            initializeRecyclerView();
            if(recyclerSelectedItems != null){
                initializeSelectedRecyclerView();
            }
        }
        calculateTotalPrice();
    }

    public void calculateTotalPrice(){
        int totalPrice = 0;
        for(int i = 0; i < MainActivity.cartItems.size(); i++){
            totalPrice = totalPrice + ((MainActivity.cartItems.get(i).getPrice()) * MainActivity.cartItems.get(i).getCartQuantity());
        }
        txtTotalPrice.setText(String.valueOf(totalPrice));
        if(txtFinalPrice != null){
            txtFinalPrice.setText(String.valueOf(totalPrice));
        }
    }

    public void successMenu(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//        final ConversationEmpty conver = deleteConversation;
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                // Inflate the custom layout/view
                View customView = inflater.inflate(R.layout.popup_checkout_success ,null);
//                    float density = MenuActivity.getResources().getDisplayMetrics().density;

                mPopupWindow = new PopupWindow(customView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

                if(Build.VERSION.SDK_INT>=21){
                    mPopupWindow.setElevation(5.0f);
                }
                Button btnOrders = (Button) customView.findViewById(R.id.btnOrders);
                btnOrders.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                 Dismiss the popup window
                        mPopupWindow.dismiss();
                        finish();
                    }
                });
                ImageView imgClose = customView.findViewById(R.id.imgClose);
                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPopupWindow.dismiss();
                        finish();
                    }
                });

                mPopupWindow.setFocusable(true);
                mPopupWindow.showAtLocation(linearLayout, Gravity.CENTER,0,0);
            }
        });
    }

    public void checkoutMenu(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//        final ConversationEmpty conver = deleteConversation;
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                // Inflate the custom layout/view
                View customView = inflater.inflate(R.layout.popup_checkout ,null);
//                    float density = MenuActivity.getResources().getDisplayMetrics().density;

                mPopupWindow = new PopupWindow(customView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

                if(Build.VERSION.SDK_INT>=21){
                    mPopupWindow.setElevation(5.0f);
                }
                Button btnBuy = (Button) customView.findViewById(R.id.btnBuy);
                btnBuy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                 Dismiss the popup window
                        mPopupWindow.dismiss();
                        successMenu();
                    }
                });
                ImageView imgClose = customView.findViewById(R.id.imgClose);
                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPopupWindow.dismiss();
//                        finish();
                    }
                });
                txtFinalPrice = customView.findViewById(R.id.txtFinalPrice);
                txtFinalPrice.setText(txtTotalPrice.getText().toString());
                recyclerSelectedItems = customView.findViewById(R.id.recyclerSelectedItems);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerSelectedItems.setLayoutManager(layoutManager);
                recyclerSelectedItems.setItemAnimator(new DefaultItemAnimator());
                recyclerSelectedItems.setAdapter(cartItemAdapter);

                mPopupWindow.setFocusable(true);
                mPopupWindow.showAtLocation(linearLayout, Gravity.CENTER,0,0);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCheckout:
                checkout();
                break;
        }
    }
}
