package com.example.barilga.Cart;

import android.os.Parcel;

import com.example.barilga.products.Product;

import java.util.ArrayList;

public class CartItem extends Product {
    private Integer cartQuantity;

    public CartItem(String productId, String storeId, String categoryId, String code, String name, String manufacturer, String brandName, String description, Integer price, Integer salePrice, String measure, String condition, Integer quantity, ArrayList<String> image, Integer rating, Boolean isApproved, Double updatedAt, Double createdAt, Integer cartQuantity) {
        super(productId, storeId, categoryId, code, name, manufacturer, brandName, description, price, salePrice, measure, condition, quantity, image, rating, isApproved, updatedAt, createdAt);
        this.cartQuantity = cartQuantity;
    }

    public CartItem(Product product, Integer cartQuantity){
        super(product.getProductId(),product.getStoreId(),product.getCategoryId(),product.getCode(),product.getName(),product.getManufacturer(),product.getBrandName(),product.getDescription(),product.getPrice(),product.getSalePrice(),product.getMeasure(),product.getCondition(),product.getQuantity(),product.getImage(),product.getRating(),product.getApproved(),product.getUpdatedAt(),product.getCreatedAt());
        this.cartQuantity = cartQuantity;
    }

    public Integer getCartQuantity() {
        return cartQuantity;
    }

    public void setCartQuantity(Integer cartQuantity) {
        this.cartQuantity = cartQuantity;
    }

    public CartItem(Parcel in) {
        super(in);
    }
}
