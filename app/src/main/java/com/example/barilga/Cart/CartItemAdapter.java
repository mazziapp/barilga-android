package com.example.barilga.Cart;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barilga.R;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.MyViewHolder> {
    private List<CartItem> cartItems;
    private Context mContext;
    private Integer type;

    public CartItemAdapter(List<CartItem> cartItems, Integer type){
        this.cartItems = cartItems;
        this.type = type;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_cart_item,viewGroup,false);
        mContext = viewGroup.getContext();
        return new CartItemAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartItemAdapter.MyViewHolder myViewHolder, int i) {
        final CartItem cartItem = cartItems.get(i);
        myViewHolder.txtQuantity.setText(cartItem.getCartQuantity().toString());
        myViewHolder.txtPrice.setText(cartItem.getPrice().toString());
        myViewHolder.txtProductName.setText(cartItem.getName());
        myViewHolder.txtDescription.setText(cartItem.getDescription());
        if(type == 1){
            myViewHolder.btnDecrease.setVisibility(View.GONE);
            myViewHolder.btnIncrease.setVisibility(View.GONE);
        }
        myViewHolder.btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext,"Increased",Toast.LENGTH_SHORT).show();
                cartItem.setCartQuantity(cartItem.getCartQuantity() + 1);
                myViewHolder.txtQuantity.setText(cartItem.getCartQuantity().toString());
                EventBus.getDefault().post(cartItem);
            }
        });
        myViewHolder.btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext,"Decreased",Toast.LENGTH_SHORT).show();
                if(cartItem.getCartQuantity() <= 1){
                    cartItems.remove(cartItem);
                    EventBus.getDefault().post(cartItem);
                }
                else{
                    cartItem.setCartQuantity(cartItem.getCartQuantity() - 1);
                    myViewHolder.txtQuantity.setText(cartItem.getCartQuantity().toString());
                    EventBus.getDefault().post(cartItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtProductName, txtPrice, txtQuantity, txtDescription;
        ImageView imgThumbnail;
        ImageButton btnDecrease, btnIncrease;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtDescription = itemView.findViewById(R.id.txtProductDescription);
            txtProductName = itemView.findViewById(R.id.txtProductName);
            txtPrice = itemView.findViewById(R.id.txtPrice);
            txtQuantity = itemView.findViewById(R.id.txtQuantity);
            btnDecrease = itemView.findViewById(R.id.btnDecrease);
            btnIncrease = itemView.findViewById(R.id.btnIncrease);
        }
    }
}
