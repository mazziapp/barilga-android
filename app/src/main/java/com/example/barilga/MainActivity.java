package com.example.barilga;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.barilga.Cart.CartItem;
import com.example.barilga.products.ProductsFragment;
import com.example.barilga.profile.ProfileFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    FrameLayout frameLayout;
    public static ArrayList<CartItem> cartItems = new ArrayList<>();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_products:
                    fragment = new ProductsFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_profile:
                    fragment = new ProfileFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        loadFragment(new HomeFragment());
        frameLayout = findViewById(R.id.frameLayout);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public static void handleCartItems(CartItem cartItem){
        boolean isContains = false;
        for (int i = 0; i < cartItems.size(); i++){
            if(cartItems.get(i).getProductId().equals(cartItem.getProductId())){
                int quantity = cartItems.get(i).getCartQuantity() + 1;
                cartItems.get(i).setCartQuantity(quantity);
                isContains = true;
                break;
            }
        }

       if(!isContains){
           cartItems.add(cartItem);
       }
    }

}
