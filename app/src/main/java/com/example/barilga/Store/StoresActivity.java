package com.example.barilga.Store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.barilga.GetStoresByOwnerIdQuery;
import com.example.barilga.R;
import com.example.barilga.profile.ProfilePreferences;
import com.example.barilga.util.MyApolloClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class StoresActivity extends AppCompatActivity {
    private RecyclerView recyclerViewStores;
    private ArrayList<Store> stores = new ArrayList<>();
    private StoreAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_stores);
        recyclerViewStores = findViewById(R.id.recyclerViewStores);
    }

    @Override
    public void onResume(){
        super.onResume();
        getStores();
    }

    public void getStores(){
        stores.clear();
        MyApolloClient.getMyApolloClient().query(GetStoresByOwnerIdQuery.builder()
        .userId(ProfilePreferences.getInstance().getId())
        .build()).enqueue(new ApolloCall.Callback<GetStoresByOwnerIdQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<GetStoresByOwnerIdQuery.Data> response) {
                try{
                    Log.v("checkGetStores","response = " + response.data().getStoresByOwnerId().toString());
                    if(response.data().getStoresByOwnerId().message().equals("OK")){
                        for(int i = 0; i < response.data().getStoresByOwnerId().result().size(); i++){
                            String storeId = response.data().getStoresByOwnerId().result().get(i)._id().toString();
                            String ownerId = response.data().getStoresByOwnerId().result().get(i).owner_id().toString();
                            String storeCode = response.data().getStoresByOwnerId().result().get(i).store_code();
                            String storeName = response.data().getStoresByOwnerId().result().get(i).name();
                            String description = response.data().getStoresByOwnerId().result().get(i).description();
                            String image = response.data().getStoresByOwnerId().result().get(i).image();
                            String email = response.data().getStoresByOwnerId().result().get(i).email();
                            String website = response.data().getStoresByOwnerId().result().get(i).website();
                            String phone = response.data().getStoresByOwnerId().result().get(i).phone();
                            String addressId = response.data().getStoresByOwnerId().result().get(i).address_id();
                            String membershipBadge = response.data().getStoresByOwnerId().result().get(i).membership_badge();
                            Double updatedAt = response.data().getStoresByOwnerId().result().get(i).updated_at();
                            Double createdAt = response.data().getStoresByOwnerId().result().get(i).created_at();
                            Store store = new Store(storeId,ownerId,storeCode,storeName,description,image,email,website,phone,addressId,membershipBadge,updatedAt,createdAt);
                            stores.add(store);
                        }
                        initializeRecyclerView();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }

    public void initializeRecyclerView(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter = new StoreAdapter(stores);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerViewStores.setLayoutManager(layoutManager);
                recyclerViewStores.setItemAnimator(new DefaultItemAnimator());
                recyclerViewStores.setAdapter(adapter);
            }
        });
    }
}
