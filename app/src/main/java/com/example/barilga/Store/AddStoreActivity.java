package com.example.barilga.Store;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.barilga.CreateStoreMutation;
import com.example.barilga.R;
import com.example.barilga.profile.ProfilePreferences;
import com.example.barilga.type.InputStore;
import com.example.barilga.util.MyApolloClient;

import org.jetbrains.annotations.NotNull;

public class AddStoreActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText inputName, inputDescription, inputAddress, inputPhone, inputEmail, inputWebsite;
    private Button btnCreateStore;
    private ImageView imgStore;

    @Override
    protected void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_add_shop);
        imgStore = findViewById(R.id.imgStore);
        inputName = findViewById(R.id.inputName);
        inputEmail = findViewById(R.id.inputEmail);
        inputPhone = findViewById(R.id.inputPhone);
        inputAddress = findViewById(R.id.inputAddress);
        inputWebsite = findViewById(R.id.inputWebsite);
        inputDescription = findViewById(R.id.inputDescription);
        btnCreateStore = findViewById(R.id.btnCreate);
        btnCreateStore.setOnClickListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void createStore(){
        InputStore inputStore = InputStore.builder().
                name(inputName.getText().toString())
                .description(inputDescription.getText().toString())
                .image("")
                .email(inputEmail.getText().toString())
                .website(inputWebsite.getText().toString())
                .phone(inputPhone.getText().toString())
                .address_id(inputAddress.getText().toString())
                .build();

        MyApolloClient.getMyApolloClient().mutate(CreateStoreMutation.builder()
        .inputStore(inputStore)
        .userId(ProfilePreferences.getInstance().getId())
        .build()).enqueue(new ApolloCall.Callback<CreateStoreMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<CreateStoreMutation.Data> response) {
                try{
                    Log.v("checkCreateStore","response = " + response.data().createStore().toString());
                    if(response.data().createStore().message().equals("OK")){
                        showToast();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }

    public void showToast(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AddStoreActivity.this,"Дэлгүүр амжилттай үүслээ",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCreate:
                createStore();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
