package com.example.barilga.Store;

public class Store {
    private String storeId;
    private String ownerId;
    private String storeCode;
    private String name;
    private String description;
    private String image;
    private String email;
    private String website;
    private String phone;
    private String addressId;
    private String membershipBadge;
    private Double updatedAt;
    private Double createdAt;

    public Store(String storeId, String ownerId, String storeCode, String name, String description, String image, String email, String website, String phone, String addressId, String membershipBadge, Double updatedAt, Double createdAt) {
        this.storeId = storeId;
        this.ownerId = ownerId;
        this.storeCode = storeCode;
        this.name = name;
        this.description = description;
        this.image = image;
        this.email = email;
        this.website = website;
        this.phone = phone;
        this.addressId = addressId;
        this.membershipBadge = membershipBadge;
        this.updatedAt = updatedAt;
        this.createdAt = createdAt;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getMembershipBadge() {
        return membershipBadge;
    }

    public void setMembershipBadge(String membershipBadge) {
        this.membershipBadge = membershipBadge;
    }

    public Double getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Double updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Double getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Double createdAt) {
        this.createdAt = createdAt;
    }
}
