package com.example.barilga.Store;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.barilga.GetProductsByStoreIdQuery;
import com.example.barilga.R;
import com.example.barilga.products.AddProductsActivity;
import com.example.barilga.products.Product;
import com.example.barilga.products.ProductAdapter;
import com.example.barilga.util.MyApolloClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ActivitySingleStore extends AppCompatActivity implements View.OnClickListener {
    private Button btnAdd;
    private RecyclerView recyclerViewProducts;
    private List<Product> products = new ArrayList<>();
    private ProductAdapter adapter;
    private String storeId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_single_store);
        storeId = getIntent().getStringExtra("storeId");
        recyclerViewProducts = findViewById(R.id.recyclerViewProducts);
        btnAdd = findViewById(R.id.btnAddProduct);
        btnAdd.setOnClickListener(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        getProducts();
    }

    public void getProducts(){
        products.clear();
        MyApolloClient.getMyApolloClient().query(GetProductsByStoreIdQuery.builder()
                .storeId(storeId)
                .build()).enqueue(new ApolloCall.Callback<GetProductsByStoreIdQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<GetProductsByStoreIdQuery.Data> response) {
                try{
                    Log.v("checkProducts","response = " + response.data().getProductsByStoreId().toString());
                    if(response.data().getProductsByStoreId().message().equals("OK")){
                        for(int i = 0; i < response.data().getProductsByStoreId().result().size(); i++){
                            String productId = response.data().getProductsByStoreId().result().get(i)._id().toString();
                            String storeId = response.data().getProductsByStoreId().result().get(i).store_id().toString();
                            String categoryId = response.data().getProductsByStoreId().result().get(i).category_id().toString();
                            String code = response.data().getProductsByStoreId().result().get(i).product_code();
                            String name = response.data().getProductsByStoreId().result().get(i).product_name();
                            String manufacturer = response.data().getProductsByStoreId().result().get(i).product_manufacturer();
                            String brandName = response.data().getProductsByStoreId().result().get(i).product_brand_name();
                            String description = response.data().getProductsByStoreId().result().get(i).product_description();
                            Integer price = response.data().getProductsByStoreId().result().get(i).product_price();
                            Integer salePrice = response.data().getProductsByStoreId().result().get(i).product_sale_price();
                            String measure = response.data().getProductsByStoreId().result().get(i).product_measure();
                            String condition = response.data().getProductsByStoreId().result().get(i).product_condition();
                            Integer quantity = response.data().getProductsByStoreId().result().get(i).product_quantity();
                            ArrayList<String> image = new ArrayList<>();
                            Integer rating = response.data().getProductsByStoreId().result().get(i).product_rating();
                            Boolean isApproved = response.data().getProductsByStoreId().result().get(i).is_approved();
                            Double updatedAt = response.data().getProductsByStoreId().result().get(i).updated_at();
                            Double createdAt = response.data().getProductsByStoreId().result().get(i).created_at();
                            Product product = new Product(productId,storeId,categoryId,code,name,manufacturer,brandName,description,price,salePrice,measure,condition,quantity,image,rating,isApproved,updatedAt,createdAt);
                            products.add(product);
                        }
                        initializeRecyclerView();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }

    public void initializeRecyclerView(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter = new ProductAdapter(products);
                RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 2);;
                recyclerViewProducts.setLayoutManager(layoutManager);
                recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
                recyclerViewProducts.setAdapter(adapter);
            }
        });
    }

    public void addProduct(){
        Intent intent = new Intent(ActivitySingleStore.this, AddProductsActivity.class);
        intent.putExtra("storeId",storeId);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAddProduct:
                addProduct();
                break;
        }
    }
}
