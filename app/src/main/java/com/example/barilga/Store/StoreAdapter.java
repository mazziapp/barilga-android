package com.example.barilga.Store;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.barilga.R;

import java.util.List;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> {
    private List<Store> stores;
    private Context mContext;

    public StoreAdapter(List<Store> stores){this.stores = stores;}

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_store,viewGroup,false);
        mContext = viewGroup.getContext();
        return new StoreAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StoreAdapter.MyViewHolder myViewHolder, int i) {
        final Store store = stores.get(i);
        myViewHolder.txtStoreName.setText(store.getName());
        myViewHolder.txtStoreAddress.setText(store.getAddressId());
        myViewHolder.itemView.setOnClickListener(new MyViewHolderOnClickListener(store,myViewHolder,mContext));
    }

    @Override
    public int getItemCount() {
        return stores.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtStoreName, txtStoreAddress;
        ImageView imgStore;

        MyViewHolder(View itemView){
            super(itemView);
            txtStoreName = itemView.findViewById(R.id.txtStoreName);
            txtStoreAddress = itemView.findViewById(R.id.txtStoreAddress);
            imgStore = itemView.findViewById(R.id.imgStore);
        }
    }

    private static class MyViewHolderOnClickListener implements View.OnClickListener{
        private final Store store;
        private MyViewHolder holder;
        private Context mContext;
        private MyViewHolderOnClickListener(Store store, MyViewHolder holder, Context mContext){
            this.holder = holder;
            this.store = store;
            this.mContext = mContext;
        }

        @Override
        public void onClick(View v){
            Intent intent = new Intent(mContext,ActivitySingleStore.class);
            intent.putExtra("storeId",store.getStoreId());
            mContext.startActivity(intent);
        }
    }
}
