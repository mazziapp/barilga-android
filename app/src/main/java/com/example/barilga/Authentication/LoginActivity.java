package com.example.barilga.Authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.barilga.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btnSignUp;
    private EditText inputPhone;
    private Integer phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_login);
        btnSignUp = findViewById(R.id.btnSignup);
        inputPhone = findViewById(R.id.inputPhone);
        btnSignUp.setOnClickListener(this);
    }

    public void openAuthenticationActivity(Integer phoneNumber){
        Intent intent = new Intent(LoginActivity.this, AuthenticationActivity.class);
        intent.putExtra("phoneNumber",phoneNumber);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignup:
                if(inputPhone.getText() != null && !inputPhone.getText().toString().isEmpty()){
                    phoneNumber = Integer.valueOf(inputPhone.getText().toString());
                    openAuthenticationActivity(phoneNumber);
                }
                else{
                    Toast.makeText(LoginActivity.this, "Утасны дугаараа оруулна уу!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
