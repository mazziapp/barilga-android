package com.example.barilga.Authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.barilga.MainActivity;
import com.example.barilga.R;
import com.example.barilga.SignUpMutation;
import com.example.barilga.profile.ProfilePreferences;
import com.example.barilga.util.MyApolloClient;

import org.jetbrains.annotations.NotNull;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnSignUp;
    private EditText inputFirstname, inputLastname, inputPassword, inputPasswordRepeat, inputEmail;
    private RadioButton radioMale, radioFemale;
    private String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_signup);
        phoneNumber = getIntent().getStringExtra("phoneNumber");
        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);
        inputEmail = findViewById(R.id.inputEmail);
        inputFirstname = findViewById(R.id.inputFirstname);
        inputLastname = findViewById(R.id.inputLastname);
        inputPassword = findViewById(R.id.inputPassword);
        inputPasswordRepeat = findViewById(R.id.inputPasswordRepeat);
        radioFemale = findViewById(R.id.radioFemale);
        radioMale = findViewById(R.id.radioMale);

    }

    public void signUp(final String nickname, final String lastname, final String password, final String gender){
        Log.v("checkSignUpResult","phone = " + phoneNumber);
        MyApolloClient.getMyApolloClient().mutate(SignUpMutation.builder()
        .email("bobo")
        .firstname(nickname)
        .lastname(lastname)
        .phone(phoneNumber)
        .role_id("idk")
        .gender(gender)
        .password(password)
        .build()).enqueue(new ApolloCall.Callback<SignUpMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<SignUpMutation.Data> response) {
                try{
                    Log.v("checkSignUpResult","response = " + response.data().signUp().toString());
                    ProfilePreferences.getInstance().putId("0000");
                    ProfilePreferences.getInstance().putNickname(nickname);
                    ProfilePreferences.getInstance().putLastname(lastname);
                    ProfilePreferences.getInstance().putPassword(password);
                    ProfilePreferences.getInstance().putPhone(phoneNumber);
                    ProfilePreferences.getInstance().putGender(gender);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
        openMainActivity();
    }

    public boolean isValid(){
        boolean isValid = true;
        if (inputEmail.getText() != null && !inputEmail.getText().toString().isEmpty() &&
                inputFirstname.getText() != null && !inputFirstname.getText().toString().isEmpty() &&
                inputLastname.getText() != null && !inputLastname.getText().toString().isEmpty() &&
                inputPassword.getText() != null && !inputPassword.getText().toString().isEmpty() &&
                inputPasswordRepeat.getText() != null && !inputPasswordRepeat.getText().toString().isEmpty()){
            if(!inputPassword.getText().toString().equals(inputPasswordRepeat.getText().toString())){
                Toast.makeText(SignupActivity.this,"Нууц үг хоорондоо таарахгүй байна!",Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(SignupActivity.this,"Бүх талбарыг бөглөнө үү!",Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        return isValid;
    }

    public void openMainActivity(){
        Intent intent = new Intent(SignupActivity.this, MainActivity.class);

        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSignUp:
                if(isValid()){
                    signUp(inputFirstname.getText().toString(),inputLastname.getText().toString(),inputPassword.getText().toString(),"male");
                }
                break;
        }
    }
}
