package com.example.barilga.Authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.barilga.MainActivity;
import com.example.barilga.R;
import com.example.barilga.SignInWithPhoneQuery;
import com.example.barilga.UpdateFcmMutation;
import com.example.barilga.profile.ProfilePreferences;
import com.example.barilga.util.MyApolloClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

public class AuthenticationActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String LOG_TAG = "AuthenticationActivity";
    private Button btnSignUp;
    private EditText inputCode;
    private TextView txtResend, txtPhoneNumber, txtError;
    private Integer phoneNumber;
    private String mFirebasePhoneNumber, mVerificationId;
    // Firebase
    private boolean mVerificationInProgress = false;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    private FirebaseMessaging firebaseMessaging;

    @Override
    protected void onCreate(Bundle savedInstances){
        super.onCreate(savedInstances);
        setContentView(R.layout.activity_authentication);
        phoneNumber = getIntent().getIntExtra("phoneNumber",0);
        btnSignUp = findViewById(R.id.btnSignup);
        txtPhoneNumber = findViewById(R.id.txtPhoneNumber);
        txtError = findViewById(R.id.txtError);
        txtResend = findViewById(R.id.txtResend);
        txtResend.setOnClickListener(this);
        inputCode = findViewById(R.id.inputConfirmationCode);
        btnSignUp.setOnClickListener(this);
        txtPhoneNumber.setText(phoneNumber.toString());
        mFirebasePhoneNumber = "+976"  + phoneNumber.toString();
        initializeFirebase();
    }

    public void initializeFirebase(){
        // Firebase
        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode("mn");
        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(LOG_TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                // updateUI(STATE_VERIFY_SUCCESS, credential);
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(LOG_TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(getApplicationContext(), getString(R.string.invalid_phone_number), Toast.LENGTH_SHORT).show();
                    // Invalid request
                    // [START_EXCLUDE]
                    txtError.setText(getString(R.string.invalid_phone_number));
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    txtError.setText(R.string.quota_exceeded);
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.quota_exceeded),
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }

//                hideLoader();
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                // Log.d(LOG_TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                txtError.setText(getString(R.string.code_sent));

                // [START_EXCLUDE]
                // Update UI
//                hideLoader();
                // updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
            }
        };
        // [END phone_auth_callbacks]

        startPhoneNumberVerification(mFirebasePhoneNumber);
    }

    private void startPhoneNumberVerification(String phoneNumber) {
//        showLoader();

        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        if (code.length() < 6) {
//            hideLoader();
            txtError.setText(getString(R.string.invalid_code));
            return;
        }

        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]

        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
//                            openMainActivity();
                            signInWithPhone();
                            // signIn();
                        } else {
//                            hideLoader();
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                txtError.setText(getString(R.string.invalid_code));
                            }
                        }
                    }
                });
    }

    public void openMainActivity(){
        Intent intent = new Intent(AuthenticationActivity.this, MainActivity.class);
        intent.putExtra("phoneNumber",String.valueOf(phoneNumber));
        startActivity(intent);
    }

    public void openSignUpActivity(){
        Intent intent = new Intent(AuthenticationActivity.this, SignupActivity.class);
        intent.putExtra("phoneNumber",String.valueOf(phoneNumber));
        startActivity(intent);
    }

    public void signInWithPhone(){
        MyApolloClient.getMyApolloClient().query(SignInWithPhoneQuery.builder()
        .phone(phoneNumber.toString())
        .build()).enqueue(new ApolloCall.Callback<SignInWithPhoneQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<SignInWithPhoneQuery.Data> response) {
                try {
                    Log.v("signWithPhone","response = " + response.data().signInWithoutPass().toString());
                    if(response.data().signInWithoutPass().message().equals("OK")){
                        ProfilePreferences.getInstance().putId(response.data().signInWithoutPass().result()._id().toString());
                        ProfilePreferences.getInstance().putNickname(response.data().signInWithoutPass().result().firstname());
                        ProfilePreferences.getInstance().putLastname(response.data().signInWithoutPass().result().lastname());
                        ProfilePreferences.getInstance().putGender(response.data().signInWithoutPass().result().gender());
                        ProfilePreferences.getInstance().putEmail(response.data().signInWithoutPass().result().email());
                        ProfilePreferences.getInstance().putToken(response.data().signInWithoutPass().result().token());
                        String fcmToken = FirebaseInstanceId.getInstance().getToken();
                        sendRegistrationToServer(fcmToken);
                        openMainActivity();
                    }
                    else if(response.data().signInWithoutPass().message().equals("Хэрэглэгч олдсонгүй")){
                        openSignUpActivity();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendRegistrationToServer(String fcmToken){
        MyApolloClient.getMyApolloClient().mutate(UpdateFcmMutation.builder()
        .userId(ProfilePreferences.getInstance().getId())
        .fcm(fcmToken)
        .build()).enqueue(new ApolloCall.Callback<UpdateFcmMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdateFcmMutation.Data> response) {
                try{
                    Log.v("checkUpdateFcm","response = " + response.data().updateFCM().toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSignup:
//                openMainActivity();
                if(inputCode.getText() != null && !inputCode.getText().toString().isEmpty()){
                    verifyPhoneNumberWithCode(mVerificationId,inputCode.getText().toString());
                }
                break;
            case R.id.txtResend:
                resendVerificationCode(mFirebasePhoneNumber, mResendToken);
                break;
        }
    }
}
