package com.example.barilga;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.barilga.Authentication.LoginActivity;
import com.example.barilga.profile.ProfilePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLContext;

public class WelcomeActivity extends AppCompatActivity {

    private static final String LOG_TAG = "WelcomeActivity";
    public SSLContext context = null;
    private LinearLayout linearLayout;
    private int PERMISSION_REQUEST = 20;
    private final String[] permissions = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        linearLayout = findViewById(R.id.linearLayout);
        ProfilePreferences.getInstance(getApplicationContext());
        launchApp();
    }


    //We are calling this method to check the permission status
    private boolean isPermissionsAllowed() {
        int result;
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }

        return true;
    }

    //Requesting permission
    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST);
    }

    private void launchApp() {
        int SPLASH_TIME_OUT = 500;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
                if (ProfilePreferences.getInstance().getId() == null || ProfilePreferences.getInstance().getId().isEmpty()) {
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                }
                else if(ProfilePreferences.getInstance().getId() != null && !ProfilePreferences.getInstance().getNickname().isEmpty()){
//                    intent = new Intent(getApplicationContext(), MenuActivity.class);
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                }

                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);

    }

}
